﻿using System;

namespace Fjord.NugetTest
{
    public class RandomNumberGenerator
    {
        private Random _random { get; }

        public RandomNumberGenerator(int seed)
        {
            _random = new Random(seed);
        }

        public int GenerateNumber(int max)
        {
            return _random.Next(max);
        }
    }
}
